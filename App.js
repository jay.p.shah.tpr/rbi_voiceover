import 'react-native-gesture-handler';
// import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Dimensions, StatusBar, ActivityIndicator, Image } from 'react-native';
import { Rtext } from './src/common/Rtext';
import { NavigationContainer } from '@react-navigation/native';
import AuthStackNav from './src/navigations/AuthStackNav';
import { SafeAreaView } from 'react-native-safe-area-context';
import React, { useEffect, useState } from 'react';
import { AllIcons, colors } from './src/assets/common/Common';
import { Provider, useSelector } from 'react-redux';
import store from './src/Store';
import MainStackNav from './src/navigations/MainStackNav';
const { height, width } = Dimensions.get('window');
import Modal from "react-native-modal";
import AuthFrame from './src/authScreens/AuthFrame';
export default function App() {
  const [splash, setSplash] = useState(true)
  const TimeOutSplash = () => {
    setTimeout(() => {
      setSplash(false)
    }, 4000);
  }
  useEffect(() => {
    TimeOutSplash()
  }, [])
  if (splash) {
    return (
      <AuthFrame>
        <View style = {{ flex :1 , alignItems :'center', justifyContent :'center'}}>
        <Image style={styles.ImageContainer} source={AllIcons.logoIcon} />
        </View>

      </AuthFrame>
      //
    )
  }
  return (<>
    <View style={styles.container}>
      <Provider store={store}>
        <MainView />
      </Provider>
    </View>
  </>
  );
}
const MainView = () => {
  const userLogedIn = useSelector(store => store?.auth?.userLogedIn);
  const loader = useSelector(store => store?.auth?.loader);
  return (<View style={{ flex: 1 }}>
    <NavigationContainer>
      {
        userLogedIn ? <MainStackNav /> : <AuthStackNav />
      }

    </NavigationContainer>
    {
      loader && <LoaderView />
    }

  </View>
  )
}


const LoaderView = () => {
  return (
    <Modal style={styles.modelContainer} isVisible={true} >
      <View style={styles.activityView}>
        <ActivityIndicator size={'large'} color={colors.appColor} />
      </View>
    </Modal>
  )
}
const styles = StyleSheet.create({
  ImageContainer: {
height : 150 , 
width : 150,
  },
  container: {
    flex: 1
  },
  modelContainer: {
    alignItems: 'center', justifyContent: 'center'
  },
  activityView: {
    height: 70,
    width: 70,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderColor: colors.appColor,
    borderWidth: 2
  }

});
