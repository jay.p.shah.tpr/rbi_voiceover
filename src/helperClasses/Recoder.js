

import { Player, Recorder } from '@react-native-community/audio-toolkit'
import { PermissionsAndroid, Platform } from 'react-native'

class RecordAudio {
    recoder : Recorder;
    filePath = "";
    async _requestRecordAudioPermission() {
        try {
            const granted = PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, {
                title: "Microphone Permissions",
                message: "rbiVoice need to access your microphone for communications.",
                buttonNeutral: 'Ask Me Later',
                buttonNegative: "cancel",
                buttonPositive: 'ok'
            }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            return false;
        }
    }
    async recordVoid() {

        let recordAudioRequest;
        if (Platform.OS === "android") {
            recordAudioRequest = this._requestRecordAudioPermission();
        }else{
            recordAudioRequest = new Promise(function(resolve , reject){ resolve(true)})
        }

        recordAudioRequest.then((hasPermisson) => {
            if (!hasPermisson) {
                alert("Need recoring access");
                return
            }
        })
    }


    async startRecording() {
        return new Promise((resolve , reject) =>{

            try{
                let recordingFileName = "simple" + ".mp4";
                let options = {
                    bitrate: 512000,
                    channels: 2,
                    sampleRate: 88200,
                    quality: 'max'
                }
        
                this.recoder = new Recorder(recordingFileName , options)
                this.recoder.prepare((err, fsPath) => {
                    this.filePath = fsPath;
                    // console.log("path ====>>>",fsPath);
                    this.recoder.record();
                })
                setTimeout(() => {
                    this.stopRecoding();
                    resolve( this.filePath)
                }, 4000);
            }catch(error){
                reject(error)
            }
          

        })
       
    }

    async stopRecoding () {
        this.recoder.stop();
        this.recoder.destroy();
    }

}



export const NewRecorder = new RecordAudio();



