import RNFetchBlob from 'rn-fetch-blob'
import { NewRecorder } from '../../helperClasses/Recoder'
import { newPlayer } from '../../helperClasses/Player'
import store from '../../Store'


export const colors = {
    white: "#fff",
    black: 'black',
    silver: "silver",
    red: 'red',
    green: 'green',
    purple: "#665df5",
    buttonBlue: "#599CF2",
    bottomActiveTintColor: '#599CF2',
    bottomInActiveTintColor: "#A6A6A6",
    appColor: "#205DB1",
    disableTxtInputColor: "#C8CDD1"
}

export const keyBoardType = {
    default: "default",
    email: 'email-address',
    numeric: "numeric",
    numberpad: "number-pad",

}


export const moneyArray = [

    //     {
    //     value : 0.50 , 
    //     image : require('../moneyicons/two.png')
    // },

    // {
    //     value : 1.00 , 
    //     image : require('../moneyicons/two.png')
    // },

    {
        value: 2.00,
        image: require('../moneyicons/two.png')
    },
    {
        value: 5.00,
        image: require('../moneyicons/five.png')
    },
    {
        value: 10.00,
        image: require('../moneyicons/ten.png')
    },
    {
        value: 20.00,
        image: require('../moneyicons/twe.png')
    },
    {
        value: 50.00,
        image: require('../moneyicons/fifty.png')
    },
    {
        value: 100.00,
        image: require('../moneyicons/hund.png')
    },
    {
        value: 200.00,
        image: require('../moneyicons/twohund.png')
    },
    {
        value: 500,
        image: require('../moneyicons/fivehund.png')
    },
    {
        value: 2000,
        image: require('../moneyicons/twoth.png')
    },
]


export const AllIcons = {
    // logoIcon: require('../image/logo.png'),
    logoIcon:{uri :"https://cdn-icons-png.flaticon.com/256/4289/4289742.png"},
    indianFlag: require('../image/indianFlag.png'),
    back: require('../icon/back.png'),
    home: require('../icon/home.png'),
    profile: require('../icon/profile.png'),
    addMoney: require('../icon/addMoney.png'),
    qr: require('../icon/qr.png'),
    bell: require('../icon/bell.png'),
    addm: require('../icon/addm.png'),
    payment: require('../icon/payment.png'),
    jio: require('../icon/jio.png'),
    airtel: require('../icon/airtel.png'),
    tata: require('../icon/tata.png'),
    zomato: require('../icon/zomato.png'),
    paisa: require('../icon/paisa.png'),
    copy: require('../icon/copy.png'),
    share: require('../icon/share.png'),
    download: require('../icon/download.png'),
    fiftyPaisa: require('../icon/50Paisa.png'),
    oneRupya: require('../icon/1rupya.png'),
    camera: require('../icon/camera.png'),
    gallery: require('../icon/gallery.png'),
    fileImage: require('../icon/fileImage.png'),
    cameraFrame: require('../icon/cameraFrame.png'),
    success: require('../icon/success.png'),
    splash: require('../image/splash.png'),
}


export const startAudioRecord = async () => {

const language =store.getState().auth.language;

console.log("store =====>>>>" , language);
    return new Promise(async (resolve, reject) => {
        try {
            let path = await NewRecorder.startRecording();
            // await newPlayer.startPlaying(path);
            let flies = [{
                name: "file",
                filename: "sample.mp4",
                data: RNFetchBlob.wrap(path,)
            },
            {
                name: 'vtt',
                data: 'true'
            },
            {
                name: 'language',
                data:language === "hindi"?"hindi" : "english"
            }
            ];
            let headers = {
                // "Content-Type": "application/octet-stream",
                'Content-Type': 'application/x-www-form-urlencoded',
            }

            console.log("record")
            console.log("flies =====>>>", RNFetchBlob.wrap(path,))
            await RNFetchBlob.fetch('post', "https://asr.iitm.ac.in/asr/v2/decode", headers, flies).then((resp) => {
                let data = JSON.parse(resp?.data);
                if (data?.transcript) {
                    resolve(data?.transcript);
                } else {
                    resolve("");
                }

            }).catch((error) => {
                console.log("error ====>>> check again", error)
                reject(error);

            })
        } catch (error) {
            console.log("error ====>>>", error)
            reject(error);
        }


    })
}

export const baseUrlRb = 'http://events.respark.iitm.ac.in:5000/rp_bank_api';
