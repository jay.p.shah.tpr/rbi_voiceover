import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { request } from '../utility/common';
import { NewRecorder } from '../helperClasses/Recoder';
import { newPlayer } from '../helperClasses/Player';
import RNFS from 'react-native-fs';
import { Buffer } from 'buffer'
import { Alert } from 'react-native';
import base64 from 'react-native-base64'
import RNFetchBlob from 'rn-fetch-blob';
let initialState = {
    userData: {},
    audioPath: "",
    userLogedIn: false,
    loader: false , 
    language: ""
}
const adharCheck = createAsyncThunk(
    'adharCheck',
    async (data, thunkAPI) => {
        //console.log('guestUserLogin', data);
        const response = await request('post', '/api/adharCheck', data);
        return response.data;
    },
);


const textToSpeech = createAsyncThunk(
    'textToSpeech',
    async (data, thunkAPI) => {
        //console.log('guestUserLogin', data);
        const response = await request('post', 'https://asr.iitm.ac.in/ttsv2/tts', data);
        return response.data;
    },
);
const subSlice = createSlice({
    name: 'auth',
    initialState: initialState,
    reducers: {
        userStackChange: (state, action) => {
            state.userLogedIn = true;
        },
        userLogout: (state, action) => {
            state.userLogedIn = false;
        },
        loaderOn: (state, action) => {
            state.loader = true;
        },
        loaderOff: (state, action) => {
            state.loader = false;
        },
        saveUserData: (state, action) => {
            state.userData = action.payload;
        },
        setLanguageHindi : (state, action) => {
            state.language = "english";
        },
        setLanguageEnglish : (state, action) => {
            state.language = "english";
        },
    },
    extraReducers: {

        [adharCheck.fulfilled]: (state, action) => {
        },
        [adharCheck.pending]: (state, action) => {
        },
        [adharCheck.rejected]: (state, action) => {

        },

        [textToSpeech.fulfilled]: (state, action) => {

            // let newData = atob(action.payload.audio)
            // console.log("action.payload.audioaction.payload.audioaction.payload.audioaction.payload.audio", action.payload)
            let audioData = base64.decode(action.payload.audio);
            var path = RNFS.TemporaryDirectoryPath + '/test.mp3';
            RNFS.writeFile(path, action.payload.audio, 'base64')
                .then(async (success) => {
                    // console.log('FILE WRITTEN!', success, path);
                    await newPlayer.startPlaying(path);
                })
                .catch((err) => {
                    console.log(err.message);
                });
        },
        [textToSpeech.rejected]: (state, action) => {
            // console.log("actionactionactionactionaction===>>", action)
        },
    },
});

export const { userStackChange, userLogout,setLanguageHindi,setLanguageEnglish, loaderOn, loaderOff  , saveUserData} = subSlice.actions;
export { adharCheck, textToSpeech };
export default subSlice.reducer;