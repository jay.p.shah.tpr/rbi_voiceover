// import { createStackNavigator } from '@react-navigation/stack';
import { createNativeStackNavigator} from '@react-navigation/native-stack'
import { View, Text, StyleSheet ,Dimensions} from 'react-native';
import { colors } from '../assets/common/Common';
import Adhar from '../authScreens/Adhar';
import PhoneNumber from '../authScreens/PhoneNumber';
import Verification from '../authScreens/Verification';
import AddMoney from '../mainScreens/AddMoney';
import AddMoneyDeno from '../mainScreens/AddMoney/AddMoneyDeno';
import DashBoard from '../mainScreens/DashBoard';
import Pay from '../mainScreens/Pay';
import QrCode from '../mainScreens/QrCode';
import QrShare from '../mainScreens/QrCodes/QrShare';
import YourQrCode from '../mainScreens/QrCodes/YourQrCode';
import SelectDenominations from '../mainScreens/SendMoney/SelectDenominations';
import SendMoney from '../mainScreens/SendMoney';
import React from 'react';
import BottomTabNav from './BottomTabNav';
import QrScanner from '../mainScreens/QrCodes/QrScanner';
import Success from '../mainScreens/Success/Success';
import SuccessNotes from '../mainScreens/Success/SucessNotes';
import Transaction from '../mainScreens/Transaction/Transaction';
const {height , width } = Dimensions.get('window')
const Stack = createNativeStackNavigator();
function MainStackNav() {
    return (
        <Stack.Navigator initialRouteName = {'BottomTabNav'}  screenOptions = {{ headerShown : false}}>
            <Stack.Screen name="BottomTabNav" component={BottomTabNav} />
            <Stack.Screen name="DashBoard" component={DashBoard} />
            <Stack.Screen name="QRCode" component={QrCode} options = {{headerShown : true , headerTintColor :colors.purple}} />
            <Stack.Screen name="AddMoney" component={AddMoney} options = {{headerShown : true , headerTintColor :colors.purple}} />
            <Stack.Screen name="SendMoney" component={SendMoney} options = {{headerShown : true , headerTintColor :colors.purple}} />
            <Stack.Screen name="Pay" component={Pay} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="SelectDenominations" component={SelectDenominations} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="QrShare" component={QrShare} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="YourQrCode" component={YourQrCode} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="AddMoneyDeno" component={AddMoneyDeno} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="QrScanner" component={QrScanner} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="Success" component={Success} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="SuccessNotes" component={SuccessNotes} options = {{headerShown : false , headerTintColor :colors.purple}} />
            <Stack.Screen name="Transaction" component={Transaction} options = {{headerShown : false , headerTintColor :colors.purple}} />
            
        </Stack.Navigator>
    );
}

export default MainStackNav;
const styles = StyleSheet.create({
    container: {
   height : height , 
   width : width,
    
    },
  });