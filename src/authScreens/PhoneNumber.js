import React, { useState } from 'react'
import { View, Text, Image, StyleSheet, Dimensions, Alert, PermissionsAndroid } from 'react-native'
import { Rtext } from '../common/Rtext';
import { Ainput } from '../common/Ainput'
import AuthFrame from './AuthFrame'
import { AllIcons, colors, keyBoardType, startAudioRecord } from '../assets/common/Common';
import { CusButtom } from '../common/CusButtom';
import { useNavigation } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { NewRecorder } from '../helperClasses/Recoder';
import { useDispatch, useSelector } from 'react-redux';
import { loaderOff, loaderOn, saveUserData, textToSpeech } from '../Store/AuthReducer';
import { useEffect } from 'react';
import { userStackChange } from '../Store/AuthReducer';
import { request } from '../utility/common';
import Pin from '../mainScreens/Pin/Pin';
const { height, width } = Dimensions.get('window');

export default function PhoneNumber() {
    const language = useSelector(store => store?.auth?.language
    );
    const LanguageCheck = () => {
        return language === "hindi";
    }
    const dispatch = useDispatch();
    const [showPinModel, setPinModel] = useState(false)
    const [userName, setUserName] = useState("");
    const [Password, setPassword] = useState("");
    // const newRecorder = NewRecorder() 





    const checkNull = (resp) => {
        if (resp === null || resp === undefined || resp === "") {
            return "";
        } else {
            return resp;
        }

    }

    const audioPlayandSetFileds = () => {
        startAudioRecord().then((resp) => {
            console.log("resp===>", resp)
            if (resp.includes("<") || resp === "") {
                dispatch(textToSpeech({
                    "input": "Speech Not detected please try again",
                    "gender": "male",
                    "lang": "English",  
                    "alpha": 1,
                    "segmentwise": "True"
                }))
            } else {
                setUserName(checkNull(resp));
                setPinModel(true) 
            }

            console.log("resp =====>>>>>", resp)
        }).catch((error) => {

            // getUserName()
            console.log("error =====>>>>>", error)
        })
    }
    const getUserName = () => {
        dispatch(textToSpeech({
            "input": "For login please told us your nickname only",
            "gender": "male",
            "lang": "English",
            "alpha": 1,
            "segmentwise": "True"
        })).then((resp) => {
            console.log(resp.data)
            setTimeout(() => {
                audioPlayandSetFileds();
            }, 2800);
        })

    }
    const _requestRecordAudioPermission = async () => {
        try {
            const granted = PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, {
                title: "Microphone Permissions",
                message: "rbiVoice need to access your microphone for communications.",
                buttonNeutral: 'Ask Me Later',
                buttonNegative: "cancel",
                buttonPositive: 'ok'
            }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            return false;
        }
    }

    useEffect(() => {
        _requestRecordAudioPermission()
    }, [])
    const login = (nickName = "") => {
        if (userName === "") {
            Alert.alert("Wrong name")
            return
        }
        dispatch(loaderOn())
        request('post', "http://events.respark.iitm.ac.in:5000/rp_bank_api", {
            "action": "details", "nick_name": nickName === "" ? userName : nickName
        }).then((resp) => {
            dispatch(loaderOff())
            console.log("resp.data =====>>>>", resp.data)
            if (resp.data === "None") {
                dispatch(textToSpeech({
                    "input": "you have entered wrong Nick name",
                    "gender": "male",
                    "lang": "English",
                    "alpha": 1,
                    "segmentwise": "True"
                }))
                return
            }
            dispatch(saveUserData(resp.data))
            dispatch(userStackChange());
            dispatch(textToSpeech({
                "input": "you are  successfully loged in",
                "gender": "male",
                "lang": "English",
                "alpha": 1,
                "segmentwise": "True"
            }))

        }).catch((error) => {
            dispatch(loaderOff())
            dispatch(textToSpeech({
                "input": "you have enter wrong Nick name",
                "gender": "male",
                "lang": "English",
                "alpha": 1,
                "segmentwise": "True"
            }))
        })
    }
    return (
        <SafeAreaView>
            <AuthFrame onPress={() => getUserName()} >
                <View style={{ height }}>
                    <KeyboardAwareScrollView style={{ flex: 1 }}>
                        <View style={{ height: height }}>
                            <View style={styles.logoContainer}>
                                <Image style={styles.icon} source={AllIcons.logoIcon} />
                                <Rtext style={styles.headingTxt}>{LanguageCheck() ? "आरबी बैंक" : "RB Bank"}</Rtext>
                                <Rtext style={styles.heading2}>{LanguageCheck() ? "सरकारी भुगतान द्वारा संचालित" : "Powered by Government Payments"}</Rtext>
                                <View style={styles.txtiView}>
                                    <TextInput placeholder={LanguageCheck() ? "निक नाम" : "Nick name"} value={userName} onChangeText={(val) => {
                                        setUserName(val)
                                    }} style={styles.txtInput} />
                                    {/* <TextInput placeholder={"Password"} keyboardType={keyBoardType.numeric} value={Password} onChangeText={(val) => {
                                        setPassword(val)
                                    }} style={styles.txtInput} /> */}
                                </View>
                                <CusButtom onpress={() => { userName !== "" && setPinModel(true) }} textStyle={styles.btnTxt} text={LanguageCheck() ? "लॉग इन करें" : "Login"} BTNstyle={styles.generateOtpBtn} />
                            </View>

                            <View style={{ marginLeft: "5%", flex: 1, justifyContent: 'flex-end' }}>
                                <View style={{ width: "90%", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 10 }}>
                                    <View style={{ width: (width * 0.9 - 165) / 2 - 10, height: 1, backgroundColor: colors.white }} />
                                    <Rtext style={{ width: 165, fontSize: 13, color: colors.white, marginHorizontal: 10 , textAlign :'center'}}>{LanguageCheck() ? "आरबी बैंक क्यों चुनें?" : "Why Choose RB BANK ?"}</Rtext>
                                    <View style={{ width: (width * 0.9 - 165) / 2 - 10, height: 1, backgroundColor: colors.white }} />
                                </View>
                                <Rtext style={styles.desc}>{ LanguageCheck() ? "उपयोग में आसान, कुछ ही चरणों में अपना वॉलेट बनाएं":  "Easy To Use, Create Your Wallet In Just Few Steps "}</Rtext>
                                <Rtext style={styles.desc}>{ LanguageCheck() ? "ऑनलाइन और ऑफलाइन दोनों मोड में काम करता है": "Works Both In Online And Offline Mode" } </Rtext>
                                <Rtext style={styles.desc}>{ LanguageCheck() ? "बदलते भारत का हिस्सा बनें": "Be The Part Of Changing INDIA"} </Rtext>
                                <Rtext style={styles.desc}>{ LanguageCheck() ?"आरबीआई ने डिजिटल करेंसी ऐप जारी किया": "RBI Issued Digital Currency App"} </Rtext>

                            </View>
                            <View style={{ height: 1, backgroundColor: colors.white, width: "80%", alignSelf: 'center', marginTop: 10 }} />
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: "5%", justifyContent: 'space-between', paddingVertical: 10 }}>
                                <Image style={styles.imageFlag} source={AllIcons.indianFlag} />
                                <Image style={styles.smallLogo} source={AllIcons.logoIcon} />
                                <View>
                                    <Rtext style={{ ...styles.headingTxt, marginTop: 0 }}>{LanguageCheck() ? "आरबी बैंक" : "RB Bank"}</Rtext>
                                    <Rtext style={styles.heading2}>{LanguageCheck() ? "सरकारी भुगतान द्वारा संचालित" : "Powered by Government Payments"}</Rtext>
                                </View>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </AuthFrame>
            {
                userName !== "" && showPinModel && <Pin setPinModel={setPinModel} method={login} />
            }

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    Phone: {
        marginTop: 20
    },
    icon: {
        height: 140, width: 130, resizeMode: 'contain', alignItems: 'center',
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    headingTxt: {
        fontSize: 25,
        marginTop: 12,
        color: colors.white,
        fontWeight: 'bold',
        // textAlign: 'center'
    },
    heading2: {
        fontSize: 12,
        color: colors.white,
        marginBottom: 20
    },
    walletTxt: {
        fontSize: 20,
        color: colors.white,
        fontWeight: '600',
        marginTop: 30
    }, filltxt: {
        fontSize: 14, marginTop: 8, color: colors.white, marginHorizontal: "5%"
    },
    adartxt: {
        fontSize: 14, marginTop: 25, marginBottom: 10, color: colors.white
    },
    txtInput: {
        height: 45, padding: 10, marginTop: 40, backgroundColor: colors.white, borderRadius: 5, color: colors.black
    },
    txtiView: {
        width: "90%"
    },
    generateOtpBtn: {
        width: "90%",
        height: 45,
        paddingVertical: 0,
        backgroundColor: colors.buttonBlue,
        borderColor: colors.buttonBlue,
        borderRadius: 50,
        marginTop: 25
    },
    btnTxt: {
        fontSize: 18,
        color: colors.white,
        fontWeight: '600'
    },
    desc: {
        fontSize: 12,
        color: colors.white,
        paddingTop: 5, textAlign: 'center'
    },
    imageFlag: {
        height: 45, width: 80
    },
    smallLogo: {
        height: 45,
        width: 45,
        resizeMode: 'contain'
    }
});