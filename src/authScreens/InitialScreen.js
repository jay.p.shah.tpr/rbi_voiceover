import { View, Text, StyleSheet, Image, PermissionsAndroid } from 'react-native'
import React, { useEffect } from 'react'
import AuthFrame from './AuthFrame'
import { CusButtom } from '../common/CusButtom'
import { AllIcons, colors, startAudioRecord } from '../assets/common/Common'
import { Rtext } from '../common/Rtext'
import { useNavigation } from '@react-navigation/native'
import { useDispatch, useSelector } from 'react-redux'
import { textToSpeech } from '../Store/AuthReducer'
import { useState } from 'react'

export default function InitialScreen() {
    const dispatch = useDispatch()
    const [isCalled, setIsCalled] = useState(false)
    const language = useSelector(store => store?.auth?.language
        );

        const LanguageCheck = () =>{

            return language === "hindi";
        }
    const navigation = useNavigation();



    const _requestRecordAudioPermission = async () => {
        try {
            const granted = PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, {
                title: "Microphone Permissions",
                message: "rbiVoice need to access your microphone for communications.",
                buttonNeutral: 'Ask Me Later',
                buttonNegative: "cancel",
                buttonPositive: 'ok'
            }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            return false;
        }
    }


    
  const OptionsForUser = () => {
    if (isCalled) {
      return
    }
    setIsCalled(true)

 
    dispatch(textToSpeech({
      "input": "Please select one option , for login say login,  for registration say registration , for stop say stop",
      "gender": "female",
      "lang": "English",
      "alpha": 1,
      "segmentwise": "True"
    })).then(() => {
      setTimeout(() => {
        startAudioRecord().then((resp) => {
          if (resp.includes("<") || resp === "") {
            // OptionsForUser()
            dispatch(textToSpeech({
              "input": "Audio not detected please try again.",
              "gender": "male",
              "lang": "English",
              "alpha": 1,
              "segmentwise": "True"
            }))
          } else {
            if (resp.toLocaleLowerCase().includes('login')) {
                navigation.navigate('login')
              setIsCalled(false)
            }
            else if (resp.toLocaleLowerCase().includes('registration')) {
                navigation.navigate('registration')
              setIsCalled(false)
            }
            else if (resp.toLocaleLowerCase().includes('stop')) {
              setIsCalled(false);

              dispatch(textToSpeech({
                "input": "Speech recognition is stop to start again tap on screen",
                "gender": "male",
                "lang": "English",
                "alpha": 1,
                "segmentwise": "True"
              }))
            }
            else {
              dispatch(textToSpeech({
                "input": resp + "  is a wrong option.",
                "gender": "male",
                "lang": "English",
                "alpha": 1,
                "segmentwise": "True"
              }))
              setIsCalled(false)
            }
          }
          setIsCalled(false)
          //console.log("resp =====>>>>>", resp)
        }).catch((error) => {

          OptionsForUser()
          //console.log("error =====>>>>>", error)
        })
      }, 6500);
    })


  }


  useEffect(() =>{
    _requestRecordAudioPermission()
  },[])
    return (
        <View>
            <AuthFrame onPress={() =>{
                setIsCalled(true)
              !isCalled &&  OptionsForUser()
            }}  >
                <View style={styles.container}>
                    <Image style={styles.icon} source={AllIcons.logoIcon} />
                    <Rtext style={styles.headingTxt}>{LanguageCheck() ? "आरबी बैंक": "RB Bank"}</Rtext>
                    <CusButtom onpress={() => { navigation.navigate('login') }} textStyle={styles.btnTxt} text={LanguageCheck() ? "लॉग इन करें" : "Login"} BTNstyle={styles.generateOtpBtn} />
                    <CusButtom onpress={() => { navigation.navigate('registration') }} textStyle={styles.btnTxt} text={LanguageCheck() ? "पंजीकरण" : "Registration"} BTNstyle={styles.generateOtpBtn} />      
                </View>
            </AuthFrame>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    icon: {
        height: 140, width: 130, resizeMode: 'contain', alignItems: 'center',
    },
    headingTxt: {
        fontSize: 25,
        marginTop: 12,
        color: colors.white,
        fontWeight: 'bold',
        // textAlign: 'center'
    },
    btnTxt: {
        fontSize: 18,
        color: colors.white,
        fontWeight: '600'
    },
    generateOtpBtn: {
        width: "90%",
        height: 45,
        paddingVertical: 0,
        backgroundColor: colors.buttonBlue,
        borderColor: colors.buttonBlue,
        borderRadius: 50,
        marginTop: 25
    }, headingTxt: {
        fontSize: 25,
        marginTop: 12,
        color: colors.white,
        fontWeight: 'bold',
        // textAlign: 'center'
    },
})