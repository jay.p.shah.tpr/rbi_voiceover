import { View, Text, StyleSheet, Dimensions } from 'react-native'
import React from 'react'
import AuthFrame from './AuthFrame'
import { CusButtom } from '../common/CusButtom'
import { colors } from '../assets/common/Common'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { setLanguageEnglish, setLanguageHindi } from '../Store/AuthReducer'
import { useNavigation } from '@react-navigation/native'
import Pin from '../mainScreens/Pin/Pin'
const { width, height } = Dimensions.get('window');
export default function Language() {
    const dispatch = useDispatch();
    const navigation = useNavigation()
    const [selectionIndex, setSelectionIndex] = useState(-1)
    return (
        <View>
            <AuthFrame >
                <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 20, justifyContent: 'space-between', marginTop: 80 }}>
                    <CusButtom onpress={() => {
                        setSelectionIndex(0)
                        dispatch(setLanguageEnglish())
                        setTimeout(() => {
                            navigation.navigate('InitialScreen')
                        }, 1500);
                    }}
                        textStyle={styles.langTxt}
                        BTNstyle={selectionIndex === 0 ? styles.langBtnselected : styles.langBtn} text={"English"} />
                    <CusButtom
                        textStyle={styles.langTxt}
                        onpress={() => {
                            setSelectionIndex(1)
                            dispatch(setLanguageHindi())
                            setTimeout(() => {
                                navigation.navigate('InitialScreen')
                            }, 1500);
                        }} BTNstyle={selectionIndex === 1 ? styles.langBtnselected : styles.langBtn} text={"हिंदी"} />
                </View>
            </AuthFrame>
            {/* <Pin/> */}
        </View>
    )
}

const styles = StyleSheet.create({
    langBtn: {
        height: width / 2 - 40,
        width: width / 2 - 40,
        backgroundColor: colors.appColor,

    },
    langBtnselected: {
        height: width / 2 - 40,
        width: width / 2 - 40,
        backgroundColor: colors.appColor,
        borderWidth: 4
    },
    langTxt: {
        fontSize: 18,
        fontWeight: 'bold'
    }
})