import React, { useState } from 'react'
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import { Rtext } from '../common/Rtext';
import { Ainput } from '../common/Ainput'
import AuthFrame from './AuthFrame'
import { AllIcons, colors } from '../assets/common/Common';
import { CusButtom } from '../common/CusButtom';
import { useNavigation } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput } from 'react-native-gesture-handler';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch } from 'react-redux';
import { userStackChange } from '../Store/AuthReducer';
const { height, width } = Dimensions.get('window');


const CELL_COUNT = 4;
export default function VerificationPhone({route }) {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
    return (
        <SafeAreaView>
            <AuthFrame >
                <View style={{ height }}>

                    <KeyboardAwareScrollView style={{ flex: 1 }}>
                        <View style={{ height: height }}>
                            <View style={styles.logoContainer}>
                                <Image style={styles.icon} source={AllIcons.logoIcon} />
                                <Rtext style={styles.headingTxt}>RB Bank</Rtext>
                                <Rtext style={styles.heading2}>Powered by Government Payments</Rtext>
                                <View style={styles.txtiView}>
                                    <Rtext style={styles.adartxt}>Enter Your Mobile no.</Rtext>
                                    <TextInput value={route.params.phone} style={styles.txtInput} />
                                    <Rtext style={styles.enterOtp}>Enter OTP</Rtext>
                                    <View style={{ marginHorizontal: "5%" }}>
                                        <CodeField
                                            ref={ref}
                                            {...props}
                                            value={value}
                                            onChangeText={setValue}
                                            cellCount={CELL_COUNT}
                                            rootStyle={styles.codeFieldRoot}
                                            keyboardType="number-pad"
                                            textContentType="oneTimeCode"
                                            renderCell={({ index, symbol, isFocused }) => (
                                                <Text
                                                    key={index}
                                                    style={[styles.cell, isFocused && styles.focusCell]}
                                                    onLayout={getCellOnLayoutHandler(index)}>
                                                    {symbol || (isFocused ? <Cursor /> : null)}
                                                </Text>
                                            )}
                                        />
                                    </View>
                                </View>
                                <TouchableOpacity>
                                    <Rtext style={styles.resendOtp}>Resend OTP</Rtext>
                                </TouchableOpacity>

                                <CusButtom onpress={() => {
                                    {
                                        // dispatch(userStackChange())
                                        navigation.navigate('Adhar')
                                    }
                                }} textStyle={styles.btnTxt} text={"Submit"} BTNstyle={styles.generateOtpBtn} />
                            </View>

                            <View style={{ marginLeft: "5%", flex: 1, justifyContent: 'flex-end' }}>
                                <View style={{ width: "90%", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 10 }}>
                                    <View style={{ width: (width * 0.9 - 165) / 2 - 10, height: 1, backgroundColor: colors.white }} />
                                    <Rtext style={{ width: 165, fontSize: 13, color: colors.white, marginHorizontal: 10 }}>Why Choose IndiePay ?</Rtext>
                                    <View style={{ width: (width * 0.9 - 165) / 2 - 10, height: 1, backgroundColor: colors.white }} />
                                </View>
                                <Rtext style={styles.desc}>Easy To Use, Create Your Wallet In Just Few Steps </Rtext>
                                <Rtext style={styles.desc}>Works Both In Online And Offline Mode </Rtext>
                                <Rtext style={styles.desc}>Be The Part Of Changing INDIA </Rtext>
                                <Rtext style={styles.desc}>RBI Issued Digital Currency App </Rtext>
                            </View>
                            <View style={{ height: 1, backgroundColor: colors.white, width: "80%", alignSelf: 'center', marginTop: 10 }} />
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: "5%", justifyContent: 'space-between', paddingVertical: 10 }}>
                                <Image style={styles.imageFlag} source={AllIcons.indianFlag} />
                                <Image style={styles.smallLogo} source={AllIcons.logoIcon} />
                                <View>
                                    <Rtext style={{ ...styles.headingTxt, marginTop: 0 }}>RB Bank</Rtext>
                                    <Rtext style={styles.heading2}>Powered by Government Payments</Rtext>
                                </View>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                    <TouchableOpacity style={{ position: 'absolute', top: 45, left: 20 }} onPress={() => navigation.goBack()}>
                        <Image style={{ height: 24, width: 24, resizeMode: 'contain' }} source={AllIcons.back} />
                    </TouchableOpacity>
                </View>
            </AuthFrame>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    Phone: {
        marginTop: 20
    },
    icon: {
        height: 140, width: 130, resizeMode: 'contain', alignItems: 'center',
    },
    resendOtp: {
        fontSize: 15,
        fontWeight: "600",
        color: "#FFF495",
        marginTop: 10
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },
    headingTxt: {
        fontSize: 25,
        marginTop: 12,
        color: colors.white,
        fontWeight: 'bold',
        // textAlign: 'center'
    },
    heading2: {
        fontSize: 12,
        color: colors.white
    },
    walletTxt: {
        fontSize: 22,
        color: colors.white,
        fontWeight: '600',
        marginTop: 30
    },
    filltxt: {
        fontSize: 14, marginTop: 10, color: colors.white
    },
    adartxt: {
        fontSize: 14, marginTop: 25, marginBottom: 10, color: colors.white
    },

    enterOtp: {
        fontSize: 14, marginTop: 15, marginBottom: 0, color: colors.white
    },
    txtInput: {
        height: 45, padding: 10, backgroundColor: colors.disableTxtInputColor, borderRadius: 5, color: colors.black
    },
    txtiView: {
        width: "90%"
    },
    generateOtpBtn: {
        width: "90%",
        height: 45,
        paddingVertical: 0,
        backgroundColor: colors.buttonBlue,
        borderColor: colors.buttonBlue,
        borderRadius: 50,
        marginTop: 10
    },
    btnTxt: {
        fontSize: 18,
        color: colors.white,
        fontWeight: '600'
    },
    desc: {
        fontSize: 12,
        color: colors.white,
        paddingTop: 5, textAlign: 'center'
    },
    imageFlag: {
        height: 45, width: 80
    },
    smallLogo: {
        height: 45,
        width: 45,
        resizeMode: 'contain'
    },
    root: { flex: 1, padding: 20 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFieldRoot: { marginTop: 20 },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 18,
        borderRadius: 7,
        backgroundColor: colors.white,
        textAlign: 'center',
        color: colors.black
    },
    focusCell: {
        borderColor: colors.white,
        borderRadius: 7,
        backgroundColor: colors.white
    },
});