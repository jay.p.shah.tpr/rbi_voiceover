import { View, Text, StyleSheet, Image, TextInput, Dimensions, Keyboard, Alert } from 'react-native'
import React, { useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import AuthFrame from '../authScreens/AuthFrame';
import CustomBack from '../common/CustomBack/CustomBack';
import { AllIcons, baseUrlRb, colors, keyBoardType, startAudioRecord } from '../assets/common/Common';
import { Rtext } from '../common/Rtext';
import { Ainput } from '../common/Ainput';
import { LeftHeadingTxt } from './TabScreens/Home';
import { CusButtom } from '../common/CusButtom';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { request } from '../utility/common';
import { useDispatch, useSelector } from 'react-redux';
import { textToSpeech } from '../Store/AuthReducer';
import { useIsFocused } from '@react-navigation/native';
import { newPlayer } from '../helperClasses/Player';
import { useEffect } from 'react';
import Pin from './Pin/Pin';
export default function Pay({ navigation }) {
    const [ pinModel , setPinModel] = useState(false);
    const dispatch = useDispatch();
    const isfocus = useIsFocused()
    const [currentFiledIndex, setCurrentFiledIndex] = useState(0);
    const auth = useSelector(store => store?.auth);
    const [money, setMoney] = useState("0");
    const [height, setHeight] = useState(100);
    const [isFocusMultiline, setIsFocusedMultiline] = useState(false);
    const [personName, setPersonName] = useState("");
    const [ iscLalled , setIsCalled] =useState(false)
    const checkNull = (resp) => {
        if (resp === null || resp === undefined || resp === "") {
            return "";
        } else {
            return resp;
        }

    }



    const incrementIndex = () => {
        setTimeout(() => {
            setCurrentFiledIndex(prev => prev + 1)
        }, 600);
    
    }

    const audioPlayandSetFileds = () => {

        if (currentFiledIndex === 0) {
            startAudioRecord().then((resp) => {
                if (resp.includes("<") || resp === "") {
                    getValues()
                } else {
                    incrementIndex()
                    setMoney(checkNull(resp));
                }
                console.log("resp =====>>>>>", resp)
            }).catch((error) => {
                getValues()
                console.log("error =====>>>>>", error)
            })

        } else if (currentFiledIndex === 1) {
            startAudioRecord().then((resp) => {

                if (resp.includes("<") || resp === "") {
                    getValues()
                } else {
                    incrementIndex()
                    setPersonName(checkNull(resp));

                    dispatch(textToSpeech({
                        "input": "To send money to irfan please ttap on screen ",
                        "gender": "female",
                        "lang": "English",
                        "alpha": 1,
                        "segmentwise": "True"
                    }))
                }
                console.log("resp =====>>>>>", resp)
            }).catch((error) => {
                getValues()
                console.log("error =====>>>>>", error)
            })
        }
    }



    const getValues = () => {
        setIsCalled(true)
        newPlayer.stopPlaying()
        console.log("currentFiledIndex", currentFiledIndex)
        if (personName !== "" && money !== "") {
            setPinModel(true);
            return
        }

        if (currentFiledIndex === 0) {
            dispatch(textToSpeech({
                "input": "Please told the money to send.",
                "gender": "male",
                "lang": "English",
                "alpha": 1,
                "segmentwise": "True"
            })).then((response) => {
                console.log("response==========================>>>>>>>", response)
                if (isfocus) {
                    setTimeout(() => {
                        audioPlayandSetFileds();
                    }, 1700);
                }

            })

        } else if (currentFiledIndex === 1) {
            dispatch(textToSpeech({
                "input": "select person to send money",
                "gender": "male",
                "lang": "English",
                "alpha": 1,
                "segmentwise": "True"
            })).then((response) => {
                console.log("response==========================>>>>>>>", response)
                if (isfocus) {
                    setTimeout(() => {
                        audioPlayandSetFileds();
                    }, 1800);
                }

            })
        }

    }

    // console.log("currentFiledIndex",currentFiledIndex)

    const sendMoney = () => {
        newPlayer.stopPlaying()
        console.log({
            "action": "transfer",
            "amount": parseInt(money),
            "from_user": getValuesName(),
            "to_user": personName
        })
        if( money === "" || personName === ""){
            dispatch(textToSpeech({
                "input": "entered wrong options",
                "gender": "female",
                "lang": "English",
                "alpha": 1,
                "segmentwise": "True"
            }))
            return
        }
        request('post', baseUrlRb, {
            "action": "transfer",
            "amount": parseInt(money),
            "from_user": getValuesName(),
            "to_user": personName
        }).then((resp) => {

            let data = resp.data.replace(/'/g, '"')
            if (JSON.parse(data).status === "failed") {
                successMsg(true)
            } else {
                console.log("resp=====>>", resp.data)
                setTimeout(() => {
                    navigation.goBack();
                }, 400);
                successMsg()
                Alert.alert("amount send successfully");
            }


        }).catch((error) => {
            console.log("error ==========>>>>>>>", error)
        })
    }

    const successMsg = (failed = false) => {
        dispatch(textToSpeech({
            "input": failed ? "something went wrong" : "balance transfer successfully",
            "gender": "female",
            "lang": "English",
            "alpha": 1,
            "segmentwise": "True"
        }))
    }
    const getValuesName = () => {
        let userData = auth.userData;
        userData = userData.replace(/'/g, "")
        userData = userData.replace(/{/g, "")
        userData = userData.replace(/}/g, "")
        userData = userData.trim().split(",")
        console.log("userData", userData[1].split(":")[1].trim())
        // userData[1]
        // getBalance(userData[1].split(":")[1].trim())
        return userData[1].split(":")[1].trim();

    }

    useEffect(() => {
        newPlayer.stopPlaying()
    }, [])


    useEffect(() => {
        if (currentFiledIndex !== 0 && currentFiledIndex !== 2) {
            getValues()
        }else{
            setIsCalled(false)
        }

    }, [currentFiledIndex])

    return (
        <SafeAreaView>
            <AuthFrame onPress={() => !iscLalled && getValues()}>
                <KeyboardAwareScrollView enableOnAndroid extraHeight={!isFocusMultiline ? 0 : height} extraScrollHeight={!isFocusMultiline ? 0 : height} >
                    <CustomBack name='Send Money To' />
                    <View style={styles.avtarContainer}>
                        <View style={styles.avtarView}>
                            <Image style={styles.avtarImg} source={AllIcons.logoIcon} />
                            <View style={styles.avatarTxtView}>
                                <Rtext style={styles.userTxt}>{"Send Money to : "+ personName}</Rtext>
                                <Rtext style={styles.userDesc}>#2345......</Rtext>
                            </View>
                        </View>
                    </View>
                    <Rtext style={styles.enterTxt}>Enter the amount to send</Rtext>
                    <View style={styles.whiteView}>
                        <View style={styles.inputView} >
                            <Image style={styles.paisaImg} source={AllIcons.paisa} />
                            <TextInput onEndEditing={() => money === "" && setMoney("0")} keyboardType={keyBoardType.numeric} onFocus={() => {
                                setMoney("")
                                setIsFocusedMultiline(false)
                            }} style={styles.priceInput} value={money} onChangeText={(val) => {
                                if (val.length > 8) {
                                    Alert.alert("Wrong Input", "Please enter a amount less than 1000000")
                                } else {
                                    setMoney(val === "" ? "0" : (val[0] === "0" ? val.substring(1, val.length) : val))
                                }
                            }} />
                        </View >
                        <View style={{ marginHorizontal: "7%" }}>
                            <Ainput value={personName} heading='person name' placeholder='person name' onChangeText={setPersonName} />
                        </View>

                        <LeftHeadingTxt headingStyle={styles.optionTxt} name='Add a note (optional)' />
                        <View style={{ marginHorizontal: "7%" }}>
                            <Ainput onFocus={() => setIsFocusedMultiline(true)} onContentSizeChange={e => e.nativeEvent.contentSize.height > 100 && setHeight(e.nativeEvent.contentSize.height)} numberOfLines={10} multiline={true} style={{ ...styles.note, height: height }} />
                        </View>
                        <CusButtom onpress={() => {
                       
                            setPinModel(true);
                            // if (parseFloat(money) > 0) {
                            //     navigation.navigate('SelectDenominations', { money: parseFloat(money) })
                            // }
                        }} textStyle={styles.btnTxt} text={"Send Money"} BTNstyle={styles.submitBtn} />
                    </View>

                </KeyboardAwareScrollView>
            </AuthFrame>
            {
                money !== "" && personName !== "" && pinModel &&  <Pin method={sendMoney} setPinModel={setPinModel} />
            }
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    avtarContainer: {
        height: 140,
        paddingHorizontal: "10%",
        paddingTop: 40,
    },
    avtarView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    avtarImg: {
        height: 55, width: 55, resizeMode: 'contain'
    },
    avatarTxtView: {
        marginLeft: 15
    },
    userTxt: {
        fontSize: 18, color: colors.white, fontWeight: 'bold'
    },
    userDesc: {
        color: colors.white, fontSize: 14, marginTop: 5
    },
    enterTxt: {
        fontSize: 16,
        color: colors.white,
        textAlign: 'center',
        marginBottom: 60
    },
    whiteView: {
        backgroundColor: colors.white,
    },
    inputView: {
        marginTop: -40,
        marginHorizontal: "7%",
        height: 100,
        backgroundColor: colors.white,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: 'rgba(0, 0, 0, 1)',
        shadowOpacity: 0.3,
        elevation: 5,
        shadowRadius: 15,
        shadowOffset: { width: 4, height: 4 },
    },
    paisaImg: {
        height: 35, width: 35
    },
    priceInput: {
        color: colors.black, height: 70, minWidth: 150, fontSize: 40, fontWeight: 'bold', marginLeft: 10
    },
    note: {
        // height: 100,
        backgroundColor: "#F8F8F8",
        textAlignVertical: 'top',
    },
    optionTxt: {
        marginLeft: "7%",
        fontSize: 18,
        fontWeight: 'bold', marginTop: 25
    },
    submitBtn: {
        marginLeft: "7%", width: "86%",
        height: 45,
        paddingVertical: 0,
        backgroundColor: colors.buttonBlue,
        borderColor: colors.buttonBlue,
        borderRadius: 50,
        marginTop: 140, marginBottom: 180
    },
    btnTxt: {
        fontSize: 18,
        color: colors.white,
        fontWeight: '600'
    }
})