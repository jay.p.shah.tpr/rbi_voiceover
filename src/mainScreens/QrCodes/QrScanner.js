import { View, Text, Dimensions, Alert, Image, StyleSheet, ImageBackground } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import AuthFrame from '../../authScreens/AuthFrame';
import CustomBack from '../../common/CustomBack/CustomBack';
// import { BarCodeScanner } from 'expo-barcode-scanner';
import { AllIcons, colors } from '../../assets/common/Common';
import { Rtext } from '../../common/Rtext';
import { CusButtom } from '../../common/CusButtom';
const { height, width } = Dimensions.get('window');
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { useNavigation } from '@react-navigation/native';
export default function QrScanner() {
const navigation = useNavigation()
    const onSuccess = e => {
        // Linking.openURL(e.data).catch(err =>
        //     console.error('An error occured', err)
        // );


        navigation.goBack();
        setTimeout(() => {
            Alert.alert("Scaned Successfully" ,"Value ===>>    " + e.data )
        }, 400);
      
    };
    return (
        <SafeAreaView>
            <AuthFrame containerOpacity={true}>
                <CustomBack />
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                    <Rtext style={styles.scanTxt}>Scan & Pay</Rtext>
                    <Rtext style={styles.cameraTxt}>Point the camera box at the QR Code to scan</Rtext>
                    <View style={styles.cameraMainContainer}>
                        <ImageBackground source={AllIcons.cameraFrame} imageStyle={{ height: "100%", width: "100%", }} style={styles.camerImageBack}>
                            <View style={styles.cameraContainer}>
                                <QRCodeScanner
                                    onRead={onSuccess}
                                    flashMode={RNCamera.Constants.FlashMode.off}
                                />
                            </View>
                        </ImageBackground>
                    </View>
                </View>
            </AuthFrame>
            <View style={styles.bottomView}>
                <CusButtom BTNstyle={styles.btnStyle} ImgStyle={styles.btnIconGalleryStyle} source={AllIcons.gallery} />
                <CusButtom BTNstyle={{ ...styles.btnStyle, height: 90, width: 90 }} ImgStyle={styles.btnIconCameraStyle} source={AllIcons.camera} />
                <CusButtom BTNstyle={styles.btnStyle} ImgStyle={styles.btnIconImageStyle} source={AllIcons.fileImage} />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    scanTxt: {
        fontSize: 22,
        color: colors.white,
        fontWeight: 'bold',
        marginBottom: 10, opacity: 1,
        marginTop: 130
    },

    cameraMainContainer: {
        width, alignItems: 'center', marginTop: 100, height: height - 220, backgroundColor: colors.white
    },
    camerImageBack: {
        height: width - 60, width: width - 60, marginTop: -100, borderRadius: 36, backgroundColor: colors.white, overflow: 'hidden',
    },
    cameraContainer: {
        height: width - 76, width: width - 76, marginTop: 8, overflow: 'hidden', marginLeft: 8, borderRadius: 29, backgroundColor: colors.black
    },
    cameraTxt: {
        fontSize: 16,
        color: colors.white,
        marginBottom: 20
    },
    bottomView: {
        position: 'absolute',
        height: 100,
        bottom: 60, width: width,
        right: 0,
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: "10%",
        justifyContent: 'space-between'
    },
    btnIconImageStyle: {
        height: 35,
        width: 27,
        resizeMode: 'contain',
        backgroundColor: colors.white
    },
    btnStyle: {
        height: 70,
        width: 70,
        backgroundColor: colors.white,
        opacity: 11,
        borderRadius: 100,
        shadowColor: 'rgba(0, 0, 0, 1)',
        shadowOpacity: 0.3,
        elevation: 5,
        shadowRadius: 15,
        shadowOffset: { width: 4, height: 4 },

    },
    btnIconGalleryStyle: {
        height: 34,
        width: 37,
        backgroundColor: colors.white,
        opacity: 11
    },
    btnIconCameraStyle: {
        height: 42,
        width: 48,
        backgroundColor: colors.white,
        opacity: 11
    }
})