import { View, Text, StyleSheet, Dimensions, Image } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { AllIcons, colors } from '../../assets/common/Common';
import { CusButtom } from '../../common/CusButtom';
import { Rtext } from '../../common/Rtext';
const { height, width } = Dimensions.get('window')
export default function Success({ navigation, }) {
    return (
        <SafeAreaView style={styles.contain}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View>
                    <Image style={styles.successImage} source={AllIcons.success} />
                    <Rtext style={styles.paymentTxt}>Payment of 500 to Reserve Bank of India was successful</Rtext>
                </View>
            </View>
            <CusButtom onpress={() => navigation.navigate('BottomTabNav' , {screen : "Home"})} textStyle={styles.btnTxt} text={"Done"} BTNstyle={styles.generateOtpBtn} />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    contain: {
        height, width, backgroundColor: colors.white
    },
    paymentTxt: {
        fontSize: 18, color: colors.black, paddingHorizontal: "8%", textAlign: 'center', marginTop: 20
    },
    generateOtpBtn: {
        marginLeft: "5%",
        width: "90%",
        height: 45,
        paddingVertical: 0,
        backgroundColor: colors.buttonBlue,
        borderColor: colors.buttonBlue,
        borderRadius: 50,
        marginTop: 25,
        marginBottom: 20,
    },
    btnTxt: {
        fontSize: 18,
        color: colors.white,
        fontWeight: '600'
    },
    successImage: {
        height: 220, width: 220, resizeMode: 'contain', alignSelf: 'center'
    }
})