import { View, Text, StyleSheet, Dimensions, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { AllIcons, colors } from '../../assets/common/Common';
import { CusButtom } from '../../common/CusButtom';
import { Rtext } from '../../common/Rtext';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const { height, width } = Dimensions.get('window')
export default function SuccessNotes({ navigation, route }) {
const [ selectedNote , setSelectedNote] = useState([]);

const setData =() =>{
let data = route.params.selectedNote.filter(item=> item.selectedVal !== 0)
setSelectedNote(data)
}
useEffect(() =>{
    setData()
},[])
console.log("route.params ==>>>>>",route.params.selectedNote)
    return (
        <SafeAreaView style={styles.contain}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.iconView}>
                        <Image style={styles.successImage} source={AllIcons.success} />
                        <Rtext style={styles.paymentTxt}>{`Payment of ${route.params.totalMoney} \n received successfully`}</Rtext>
                    </View>
                <KeyboardAwareScrollView>
                   
                    <View style={styles.priceViewContainer}>
                        <Rtext style={styles.paymentDetailsTxt}>Payment Details</Rtext>
                        {
                            selectedNote.map((item , index) => (
                                <View style={styles.priceView}>
                                    <MoneyView Text={item.value + "X" + item.selectedVal} />
                                    <MoneyView Text={parseFloat(item.value) * item.selectedVal } />
                                </View>
                            ))
                        }
                        <View>
                            <Rtext style={styles.amountTxt}>Total Amount</Rtext>
                            <View style={styles.selecetedMoneyView}>
                                <Image style={styles.paisaBigImg} source={AllIcons.paisa} />
                                <Rtext style={styles.selectedMoneyTxt}>{route.params.totalMoney}</Rtext>
                            </View>
                        </View>
                    </View>
                    <CusButtom onpress={() => navigation.pop(3)} textStyle={styles.btnTxt} text={"Done"} BTNstyle={styles.generateOtpBtn} />
                </KeyboardAwareScrollView>
            </View>

        </SafeAreaView>
    )
}



const MoneyView = ({ Text = "50X1" }) => {
    return (
        <View style={{ flexDirection: 'row', paddingHorizontal: "5%", alignItems: 'center' }}>
            <Image style={{ height: 15, width: 15, resizeMode: 'stretch', marginRight: 2 }} source={AllIcons.paisa} />
            <Rtext style={styles.price}>{Text}</Rtext>
        </View>

    )
}


const styles = StyleSheet.create({
    contain: {
        height, width, backgroundColor: colors.white
    },
    paymentTxt: {
        fontSize: 18, color: colors.black, paddingHorizontal: "8%", textAlign: 'center', marginTop: 20
    },
    paisaBigImg: {
        height: 40, width: 40, resizeMode: 'contain'
    },
    selectedMoneyTxt: {
        fontSize: 40, fontWeight: 'bold', marginLeft: 10, color: colors.black
    },
    amountTxt: {
        fontSize: 12,
        fontWeight: 'bold', textAlign: 'center', paddingTop: 20, marginBottom: 5
    },
    selecetedMoneyView: {
        height: 100,
        marginHorizontal: 20,
        borderColor: 'rgba(0, 0, 0, 0.05)',
        borderWidth: 1,
        borderRadius: 10,
        paddingHorizontal: 45,
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center',
        shadowColor: 'rgba(0, 0, 0, 1)',
        shadowOpacity: 0.3,
        backgroundColor: colors.white,
        elevation: 5,
        shadowRadius: 15,
        shadowOffset: { width: 4, height: 4 },
    },
    generateOtpBtn: {
        marginLeft: "10%",
        width: "80%",
        height: 45,
        paddingVertical: 0,
        backgroundColor: colors.buttonBlue,
        borderColor: colors.buttonBlue,
        borderRadius: 50,
        marginTop: 50,
        marginBottom: 20,
    },
    iconView: {
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60
    },
    btnTxt: {
        fontSize: 18,
        color: colors.white,
        fontWeight: '600'
    },
    successImage: {
        height: 200, width: 200, resizeMode: 'contain', alignSelf: 'center'
    },
    price: {
        fontSize: 18,
        color: colors.black,
        marginLeft: 10
    },
    priceViewContainer: {
        alignItems: 'center', justifyContent: 'center', width: width
    },
    priceView: {
        paddingTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: "100%", paddingHorizontal: "5%"
    },
    paymentDetailsTxt: {
        marginTop: 30,
        borderBottomColor: colors.black,
        borderBottomWidth: 1
    }
})