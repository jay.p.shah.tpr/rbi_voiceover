import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, Dimensions } from 'react-native'
import React from 'react'
import AuthFrame from '../../authScreens/AuthFrame'
import CustomBack from '../../common/CustomBack/CustomBack'
import { Rtext } from '../../common/Rtext'
import { baseUrlRb, colors } from '../../assets/common/Common'
import { request } from '../../utility/common'
import { useState } from 'react'
import { useEffect } from 'react'
import moment from 'moment/moment'
import { useDispatch, useSelector } from 'react-redux'
import { loaderOff, loaderOn } from '../../Store/AuthReducer'
const { width, height } = Dimensions.get('window');
export default function Transaction() {
    const dispatch = useDispatch()
    const [getAllPData, setAllPData] = useState([])
    const auth = useSelector(store => store?.auth);
    const [userData, setUserData] = useState({})
    const getUserData = () => {
        let data = auth.userData.replace(/'/g, '"');
        data = data.replace(/ObjectId/, '');
        data = data.replace(/\)/, '');
        data = data.replace(/\(/, '');
        getTransaction(JSON.parse(data).nick_name)
    }
    const getTransaction = (name) => {
        dispatch(loaderOn())
        request('post', baseUrlRb, {
            "action": "history",
            "nick_name": name
        }).then((resp) => {
            dispatch(loaderOff())
            console.log("resp====>>>>>>>>>>>>>", resp.data)
            let data = resp.data.replace(/'/g, '"');
            data = data.replace(/ObjectId/g, '');
            data = data.replace(/\)/g, '');
            data = data.replace(/\(/g, '');
            console.log("data =========>>>>>>", data)
            data = JSON.parse(data)
            setAllPData(data.slice(0, 10));
        }).catch((error) => {
            dispatch(loaderOff())
            console.log("error ====>>>", error)
        })
    }




    useEffect(() => {
        getUserData()
    }, [])
    return (
        <View>
            <AuthFrame>
                <CustomBack name='Transactions' />

                <FlatList style={{ marginTop: 20 }} data={[...getAllPData]} renderItem={({ item, index }) => (<RenderItem item={item} />)} />
            </AuthFrame>
        </View>
    )
}

const RenderItem = ({ item }) => {
    console.log("item=====>>", item)
    return (
        <TouchableOpacity style={styles.transactionContainer}>
            <Image style={styles.avtarIcon} source={{ uri: 'https://cdn-icons-png.flaticon.com/256/552/552848.png' }} />
            <View style={{ marginLeft: 10, }}>
                <Rtext style={styles.name}>{item['to-from']}</Rtext>
                <Rtext style={styles.time}>{moment(item.date
                    + " " + item.time, "YYYY-MM-DD HH:mm:ss").format('lll')}</Rtext>

            </View>
            <Rtext style={styles.absPrice}>₹{item.amount}</Rtext>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    transactionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
        backgroundColor: colors.white,
        marginHorizontal: 20,
        paddingVertical: 10, paddingHorizontal: 10, borderRadius: 10
    },
    avtarIcon: {
        height: 35,
        width: 35, resizeMode: 'contain'
    },
    name: {
        fontSize: 16,
        fontWeight: '400'
    },
    time: {
        fontSize: 10,

    },
    absPrice: {
        fontSize: 18,
        position: 'absolute',
        top: 10,
        right: 20
    }
})