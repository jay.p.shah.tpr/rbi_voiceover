import { View, Pressable, Text, StyleSheet, Image, Alert } from 'react-native'
import React, { useState } from 'react'
import AuthFrame from '../../authScreens/AuthFrame'
import { AllIcons, baseUrlRb, colors, startAudioRecord } from '../../assets/common/Common'
import { Rtext } from '../../common/Rtext'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { CusButtom } from '../../common/CusButtom'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { request } from '../../utility/common'
import { loaderOff, loaderOn, textToSpeech } from '../../Store/AuthReducer'
export default function Home() {
    const dispatch = useDispatch();
    const language = useSelector(store => store?.auth?.language
    );
    const LanguageCheck = () => {
        return language === "hindi";
    }
    const [userData, setUserData] = useState({})
    const [isCalled, setIsCalled] = useState(false)
    const isfocus = useIsFocused()
    const [balance, setBalance] = useState("0")
    const auth = useSelector(store => store?.auth);
    const [topMenu, setTopMenu] = useState([
        {
            name: LanguageCheck() ? "पैसे जोड़ें" : "Add Money",
            image: AllIcons.payment,
            color: "#205DB1"
        },
        {
            name: LanguageCheck() ? "भुगतान करना" : "Pay",
            image: AllIcons.addm,
            color: "#D74040"
        },
        {
            name: LanguageCheck() ? "पाना" : "Receive",
            image: AllIcons.addm,
            color: "#6FCC61"
        }])


    const accountBalance = () => {
        dispatch(textToSpeech({
            "input": "your account balnce is " + balance,
            "gender": "female",
            "lang": "English",
            "alpha": 1,
            "segmentwise": "True"
        }))
    }
    const navigation = useNavigation();
    const getBalance = (name) => {
        console.log({
            action: "balance",
            nick_name: name
        })
        dispatch(loaderOn())
        request('post', baseUrlRb, {
            "action": "balance",
            "nick_name": name
        }).then((resp) => {
            dispatch(loaderOff())
            console.log("resp ====>1", (resp.data))
            let balanceData = resp.data;
            balanceData = balanceData.replace(/'/g, "");
            balanceData = balanceData.replace(/{/g, "")
            balanceData = balanceData.replace(/}/g, "").trim()
            console.log("resp ====>", balanceData.split(":")[1])
            setBalance(balanceData.split(":")[1])
        }).catch((error) => {
            dispatch(loaderOff())
            console.log("error", error)

        })
    }
    useEffect(() => {
        let data = auth.userData.replace(/'/g, '"');
        data = data.replace(/ObjectId/, '');
        data = data.replace(/\)/, '');
        data = data.replace(/\(/, '');
        console.log("data", getBalance(JSON.parse(data).nick_name)
        )
        setUserData(JSON.parse(data));
    }, [isfocus]);


    const OptionsForUser = () => {
        if (isCalled) {
            return
        }
        setIsCalled(true)
        dispatch(textToSpeech({
            "input": "Please select one option , for check balance say balance , previous transection say transaction , for send money say send money. for stop say stop",
            "gender": "female",
            "lang": "English",
            "alpha": 1,
            "segmentwise": "True"
        })).then(() => {
            setTimeout(() => {
                startAudioRecord().then((resp) => {
                    if (resp.includes("<") || resp === "") {
                        // OptionsForUser()
                        dispatch(textToSpeech({
                            "input": "Audio not detected please try again.",
                            "gender": "male",
                            "lang": "English",
                            "alpha": 1,
                            "segmentwise": "True"
                        }))
                    } else {
                        if (resp.toLocaleLowerCase().includes('balance')) {
                            accountBalance();
                            setIsCalled(false)
                        } else if (resp.toLocaleLowerCase().includes('transaction')) {
                            navigation.navigate('Transaction');
                            setIsCalled(false)
                        }
                        else if (resp.toLocaleLowerCase().includes('money')) {
                            navigation.navigate("Pay");
                            setIsCalled(false)
                        }
                        else if (resp.toLocaleLowerCase().includes('stop')) {
                            setIsCalled(false)
                            dispatch(textToSpeech({
                                "input": "Speech recognition is stop to start again tap on screen",
                                "gender": "male",
                                "lang": "English",
                                "alpha": 1,
                                "segmentwise": "True"
                              }))
                        }
                        else {
                            dispatch(textToSpeech({
                                "input": resp + "  is a wrong option.",
                                "gender": "male",
                                "lang": "English",
                                "alpha": 1,
                                "segmentwise": "True"
                            }))
                            setIsCalled(false)
                        }
                    }

                    console.log("resp =====>>>>>", resp)
                }).catch((error) => {

                    OptionsForUser()
                    console.log("error =====>>>>>", error)
                })
            }, 9800);
        })


    }


    
    return (
        <SafeAreaView>
            <AuthFrame onPress={() => OptionsForUser()} >
                <View>
                    <KeyboardAwareScrollView  >
                        <View style={styles.mainContainer}>
                            <Rtext style={styles.greeting}>{LanguageCheck() ? "शुभ प्रभात  " + userData.nick_name : "Good Morning," + userData.nick_name}</Rtext>
                            <Rtext style={styles.wallet}>{LanguageCheck() ? "आपका वॉलेट बैलेंस" : "Your Wallet Balance"}</Rtext>
                        </View>
                        <View style={{ flexDirection: 'row', paddingHorizontal: "5%" }}>
                            <Image style={{ height: 24, width: 24, resizeMode: 'stretch', marginTop: 20, tintColor: colors.white }} source={AllIcons.paisa} />
                            <Rtext style={styles.price}>{balance}</Rtext>
                        </View>

                        <View style={{ flex: 1, backgroundColor: colors.white, marginTop: 100, height: 600 }}>
                            <View style={styles.card}>
                                {
                                    topMenu.map((item, index) => (
                                        <Pressable onPress={() => {
                                            if (item.name === "Pay") {
                                                navigation.navigate("Pay")
                                            } else if (item.name === "Add Money") {
                                                // navigation.navigate("AddMoneyy")
                                                Alert.alert("More to come", "This screens needs to be design soon")
                                            } else {
                                                Alert.alert("More to come", "This screens needs to be design soon")
                                            }

                                        }} style={{ backgroundColor: item.color, height: 90, width: "25%", padding: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 15 }}>
                                            <Image style={styles.menuIcon} source={item.image} />
                                            <Rtext style={styles.innTxt}> {item.name}</Rtext>
                                        </Pressable>
                                    ))
                                }
                            </View>
                            <View style={{ paddingHorizontal: "5%", marginTop: 30 }}>
                                <LeftHeadingTxt name={LanguageCheck() ? "लेन-देन" : "Transaction"} />
                                <CusButtom onpress={() => navigation.navigate('Transaction')} BTNstyle={{ backgroundColor: colors.appColor, borderRadius: 100, }} text={LanguageCheck() ? "सभी लेन-देन देखें" : "See All transaction"} />
                            </View>
                        </View>

                    </KeyboardAwareScrollView>
                </View>
            </AuthFrame>
        </SafeAreaView>
    )
}


const NoDataFound = () => {
    return (
        <View style={{ height: 100, width: "100%", alignItems: 'center', justifyContent: 'center' }}>
            <LeftHeadingTxt name={"No Transactions Found"} />
        </View>
    )
}

export const LeftHeadingTxt = ({ name = "", headingStyle }) => {
    return (
        <View>
            <Rtext style={{ ...styles.headingTxt, ...headingStyle }}>{name}</Rtext>
        </View>
    )
}


const styles = StyleSheet.create({
    smallLogo: {
        height: 35, width: 35, resizeMode: 'contain',
    },
    iconBtn: {
        backgroundColor: colors.white,
        borderRadius: 5,
        borderWidth: 0,
        paddingVertical: 0, height: 40, width: 40

    },
    headingTxt: {
        fontSize: 22,
        fontWeight: '600'
    },
    cardImg: {
        height: 70, width: 70, resizeMode: 'contain'
    },
    menuIcon: {
        height: 40, width: 40, resizeMode: 'contain'
    },
    card: {
        height: 112,
        width: "90%",
        marginLeft: "5%"
        ,
        borderRadius: 10,
        borderColor: 'silver',
        borderWidth: 1,
        marginTop: -56,
        backgroundColor: colors.white,
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'space-evenly'
    },
    innTxt: {
        fontSize: 14,
        height: 38,
        color: colors.white, textAlign: 'center'
    },
    price: {
        fontSize: 40,
        fontWeight: '500',
        color: colors.white, marginLeft: 10
    },
    iconImg: {
        height: 40, width: 40, borderRadius: 40,
    },
    haderView: {
        paddingHorizontal: "5%",
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between', marginTop: 10
    },
    desc: {
        fontSize: 12,

        color: colors.white
    },
    heading: {
        fontSize: 17,
        fontWeight: 'bold', color: colors.white
    },
    mainContainer: {
        paddingHorizontal: "5%",
        paddingVertical: 10,
    },
    greeting: {
        fontSize: 22,
        fontWeight: 'bold',
        color: colors.white
    },
    wallet: {
        fontSize: 17,
        color: colors.white, marginTop: 25, fontWeight: '300'
    }
})