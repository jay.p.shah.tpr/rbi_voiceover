import { View, Text, Modal, StyleSheet, Image } from 'react-native'
import React, { useEffect } from 'react'
import AuthFrame from '../../authScreens/AuthFrame'
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { useState } from 'react';
import { AllIcons, colors } from '../../assets/common/Common';
import { CusButtom } from '../../common/CusButtom';
import { textToSpeech } from '../../Store/AuthReducer';
import { useDispatch, useSelector } from 'react-redux';
const CELL_COUNT = 6;
export default function Pin({ method, setPinModel }) {
    const [value, setValue] = useState('');
 
    const language = useSelector(store => store?.auth?.language
        );
        const LanguageCheck = () => {
            return language === "hindi";
        }


  
    const dispatch = useDispatch()
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });

    const pinAskVoice =() =>{
        dispatch(textToSpeech({
            "input": "Please enter the pin code",
            "gender": "female",
            "lang": "English",
            "alpha": 1,
            "segmentwise": "True"
        }))
    }


    useEffect(() =>{
        pinAskVoice()
    },[])
    return (
        <Modal>
            <AuthFrame>
                <View style={{ marginHorizontal: 20, flex: 1, justifyContent: 'center' }}>
                    <Image source={AllIcons.logoIcon} style={{ height: 200, width: 200, alignSelf: 'center' }} />
                    <CodeField
                        ref={ref}
                        {...props}
                        value={value}
                        onChangeText={setValue}
                        cellCount={CELL_COUNT}
                        rootStyle={styles.codeFieldRoot}
                        keyboardType="number-pad"
                        textContentType="oneTimeCode"
                        renderCell={({ index, symbol, isFocused }) => (
                            <Text
                                key={index}
                                style={[styles.cell, isFocused && styles.focusCell]}
                                onLayout={getCellOnLayoutHandler(index)}>
                                {symbol || (isFocused ? <Cursor /> : null)}
                            </Text>
                        )}
                    />
                    <CusButtom onpress={() => {
                        if (value === "123456") {
                            setPinModel(false)
                            method();
                        } else {
                            setValue("");
                            dispatch(textToSpeech({
                                "input": "you have enter wrong pin",
                                "gender": "male",
                                "lang": "English",
                                "alpha": 1,
                                "segmentwise": "True"
                            }))
                        }
                    }} text={LanguageCheck()?  "जमा करना": "Submit"} BTNstyle={styles.btnStyles} />
                    <View style={{ marginBottom: 200 }} />
                </View>
            </AuthFrame>
        </Modal>
    )
}

const styles = StyleSheet.create({
    codeFieldRoot: { marginTop: 20 },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 18,
        borderRadius: 7,
        backgroundColor: colors.white,
        textAlign: 'center',
        color: colors.black
    },
    focusCell: {
        borderColor: colors.white,
        borderRadius: 7,
        backgroundColor: colors.white
    },
    btnStyles: {
        marginTop: 40, width: "80%", alignSelf: 'center', borderRadius: 100, backgroundColor: colors.appColor
    }
})